<?php


namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Employee extends Command
{
    protected static $defaultName = 'company:employee ';
    /**
     * @var \App\Services\Employee
     */
    private $employee;

    public function __construct(\App\Services\Employee $employee)
    {
        parent::__construct();
        $this->employee = $employee;
    }

    protected function configure()
    {
        $this
            // ...
            //->addArgument('employeeType', InputArgument::REQUIRED, 'Whom are we checking?')
            ->addArgument('employeeSkill', InputArgument::OPTIONAL, 'Get skill')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    //    $empType = $input->getArgument('employeeType');

        $empSkill = $input->getArgument('employeeSkill');
//        if ($empSkill) {
//
//        }
//        else {
            $resp = $this->employee->stringSkill($empSkill);
        //}

        $output->writeln($resp);
    }
}