<?php


namespace App\Employee;


class Manager extends Employee
{
    private $skillA  = [ '- set tasks'];

    /**
     * @return array
     */
    public function getSkillA(): array
    {
        return $this->skillA;
    }

    public function setTasks()
    {
        return true;
    }

}