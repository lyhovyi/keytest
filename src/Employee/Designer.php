<?php


namespace App\Employee;


class Designer extends Employee
{
    private $skillA  = ['- draw','- communication with manager'];

    /**
     * @return array
     */
    public function getSkillA(): array
    {
        return $this->skillA;
    }


    public function draw()
    {
        return true;
    }

    public function communicationWithManager()
    {
        return true;
    }


}