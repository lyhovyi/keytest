<?php


namespace App\Employee;


class Tester extends Employee
{
    private $skillA  = ['- code testing', '- set tasks','- communication with manager'];

    /**
     * @return array
     */
    public function getSkillA(): array
    {
        return $this->skillA;
    }
    public function codeTesting()
    {
        return true;
    }

    public function setTasks()
    {
        return true;
    }

    public function communicationWithManager()
    {
        return true;
    }


}