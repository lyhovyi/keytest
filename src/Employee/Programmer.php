<?php


namespace App\Employee;


class Programmer extends Employee
{
    private $skillA  = ['- code writing','- code testing','- communication with manager'];

    /**
     * @return array
     */
    public function getSkillA(): array
    {
        return $this->skillA;
    }


    public function codeWriting()
    {
        return true;
    }

    public function codeTesting()
    {
        return true;
    }

    public function communicationWithManager()
    {
        return true;
    }


}