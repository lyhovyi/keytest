<?php


namespace App\Services;


use App\Employee\Designer;
use App\Employee\Manager;
use App\Employee\Programmer;
use App\Employee\Tester;
use http\Client\Request;

class Employee
{
    /**
     * @var Programmer
     */
    private $programmer;
    /**
     * @var Designer
     */
    private $designer;
    /**
     * @var Tester
     */
    private $tester;
    /**
     * @var Manager
     */
    private $manager;

    public function __construct(Designer $designer, Programmer $programmer, Manager $manager, Tester $tester)
    {
        $this->designer = $designer;
        $this->programmer = $programmer;
        $this->manager = $manager;
        $this->tester = $tester;
    }
    public function stringSkill(string $request)
    {
        $mess='';
        $messages = $this->$request->getSkillA();
        foreach ($messages as $message_item):
            $mess.= $message_item;
            endforeach;
        //$messages = $request;
        return $mess;
    }

    public function can(string $typeEmployee, string $checkSkill)
    {
        $messages = method_exists($this->$typeEmployee,$checkSkill);
        return $messages;
    }

}